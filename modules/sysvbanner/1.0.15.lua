help([[
System-V banner clone: Displays a `banner' text the same way as the System V banner does: horizontally.
]])
whatis("Version: 1.0.15")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")

prepend_path("MANPATH", pathJoin(RpdApps, "sysvbanner-1.0.15/man"))

d = pathJoin(RpdApps, "sysvbanner-1.0.15/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end


