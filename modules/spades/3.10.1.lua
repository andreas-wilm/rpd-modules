help([[
SPAdes Genome Assembler
]])
whatis("Version: 3.10.1")
whatis("URL: http://cab.spbu.ru/software/spades/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "spades-3.10.1/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
