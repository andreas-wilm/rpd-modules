help([[
verifyBamID is a software that verifies whether the reads in particular file match previously known genotypes for an individual (or group of individuals), and checks whether the reads are contaminated as a mixture of two samples
]])
whatis("Version: 1.1.2")
whatis("URL: http://genome.sph.umich.edu/wiki/VerifyBamID")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "verifyBamID-1.1.2/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
