help([[
MUMmer is a system for rapidly aligning entire genomes, whether in complete or draft form
]])
whatis("Version: 3.23")
whatis("URL: http://mummer.sourceforge.net/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "mummer-3.23/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
