help([[
RSEM (RNA-Seq by Expectation-Maximization)
]])
whatis("Version: 1.3.0")

whatis("URL: https://deweylab.github.io/RSEM/")
load("R")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "rsem-1.3.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
