help([[
RSEM (RNA-Seq by Expectation-Maximization)
]])
whatis("Version: 1.2.31")

-- whatis("URL: FIXME")
load("R")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "rsem-1.2.31/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
