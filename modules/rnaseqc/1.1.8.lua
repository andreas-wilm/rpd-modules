help([[
RNA-SeQC computes a series of quality control metrics for RNA-seq data
]])
whatis("Version: 1.1.8")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "rnaseqc-1.1.8/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
