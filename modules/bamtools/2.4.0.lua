help([[
bamtools: C++ API & command-line toolkit for working with BAM data
]])
whatis("Version: 2.4.0")
whatis("URL: https://github.com/pezmaster31/bamtools")

RpdApps = os.getenv("RPD_APPS")

d = pathJoin(RpdApps, "bamtools-2.4.0/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
prepend_path("LD_LIBRARY_PATH", pathJoin(RpdApps, "bamtools-2.4.0/lib"))
