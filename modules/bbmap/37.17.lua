help([[
BBTools is a suite of fast, multithreaded bioinformatics tools designed for analysis of DNA and RNA sequence data.
]])
whatis("Version: 37.17")
whatis("URL: http://jgi.doe.gov/data-and-tools/bbtools/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/envs/bbmap-37.17/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
