help([[
MetaPhlAn2 - An enhanced metagenomic taxonomic profiling tool
]])
whatis("Version: 2.6.0")
whatis("URL: http://segatalab.cibio.unitn.it/tools/metaphlan2/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/envs/metaphlan2-2.6.0-and-humann2-0.9.9/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
