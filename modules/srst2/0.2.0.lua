help([[
Short Read Sequence Typing for Bacterial Pathogens
]])
whatis("Version: 0.2.0")
whatis("URL: https://github.com/katholt/srst2")
whatis("Note: This is actually a conda environment, i.e. it includes other programs")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/envs/srst2-0.2.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

-- try to recove from cases where other modules get loaded and thus change default samtools/bowtie.
-- cannot recover from python path changes though
setenv("SRST2_SAMTOOLS",  pathJoin(RpdApps, "miniconda3/envs/srst2-0.2.0/bin/samtools"))
setenv("SRST2_BOWTIE2",  pathJoin(RpdApps, "miniconda3/envs/srst2-0.2.0/bin/bowtie2"))
setenv("SRST2_BOWTIE2_BUILD",  pathJoin(RpdApps, "miniconda3/envs/srst2-0.2.0/bin/bowtie2-build"))
        
