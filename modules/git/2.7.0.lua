help([[
Git is a free and open source distributed version control system
]])
whatis("Version: 2.7.0")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")

prepend_path("MANPATH", pathJoin(RpdApps, "git-2.7.0/share/man"))

d =  pathJoin(RpdApps, "git-2.7.0/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
