help([[
MuTect is a method developed at the Broad Institute for the reliable and accurate identification of somatic point mutations in next generation sequencing data of cancer genomes.
]])
whatis("Version: 1.1.7")
whatis("URL: http://archive.broadinstitute.org/cancer/cga/mutect")

load("java/1.7")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/muTect-1.1.7/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
