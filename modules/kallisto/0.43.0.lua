help([[
kallisto is a program for quantifying abundances of transcripts from RNA-Seq data
]])
whatis("Version: 0.43.0")
whatis("URL: https://pachterlab.github.io/kallisto/about")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "kallisto_linux-v0.43.0/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
