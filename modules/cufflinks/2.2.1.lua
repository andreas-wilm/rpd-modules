help([[
Cufflinks Transcriptome assembly and differentialexpression analysis for RNA-Seq
]])
whatis("Version: 2.2.1")
-- whatis("URL: FIXME")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "cufflinks-2.2.1/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
