help([[
Kraken: a system for assigning taxonomic labels to short DNA sequences
]])
whatis("Version: 0.10.6_eaf8fb680")
whatis("URL: https://ccb.jhu.edu/software/kraken/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "kraken-0.10.6_eaf8fb68/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
