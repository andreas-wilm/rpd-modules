help([[
GNU datamash is a command-line program which performs basic numeric,textual and statistical operations on input textual data files
]])
whatis("Version: 1.1.0")
whatis("URL: https://www.gnu.org/software/datamash/")

RpdApps = os.getenv("RPD_APPS")
prepend_path("MANPATH", pathJoin(RpdApps, "datamash-1.1.0/share/man/"))
d = pathJoin(RpdApps, "datamash-1.1.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
