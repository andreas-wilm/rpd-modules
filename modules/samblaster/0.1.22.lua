help([[
samblaster: a tool to mark duplicates and extract discordant and split reads from sam files.
]])
whatis("Version: 0.1.22")
whatis("URL: https://github.com/GregoryFaust/samblaster")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "samblaster-v.0.1.22/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
