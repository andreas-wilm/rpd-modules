help([[
pigz: A parallel implementation of gzip for modern multi-processor, multi-core machines
]])
whatis("Version: 2.3.4")

whatis("URL: http://zlib.net/pigz/")
RpdApps = os.getenv("RPD_APPS")


d =  pathJoin(RpdApps, "pigz-2.3.4/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

prepend_path("MANPATH", pathJoin(RpdApps, "pigz-2.3.4/share/man"))
	
