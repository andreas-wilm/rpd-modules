help([[
Reading/writing/editing/indexing/viewing SAM/BAM/CRAM format
]])
whatis("Version: 1.3")
whatis("URL: http://www.htslib.org/")

-- for bamstats
load("gnuplot/5.0.3")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/samtools-1.3/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
