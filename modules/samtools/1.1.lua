help([[
Samtools is a suite of programs for interacting with high-throughput sequencing data.
]])
whatis("Version: 1.1")
whatis("URL: http://www.htslib.org/")

RpdApps = os.getenv("RPD_APPS")
prepend_path("MANPATH", pathJoin(RpdApps, "samtools-1.1/share/man/"))
d = pathJoin(RpdApps, "samtools-1.1/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
