help([[
Single Cell RNAseq pipeline readtransforming using umis, alignment, gene-deduplication by umi-tools and counting
]])
whatis("Version: 0.2.0")
whatis("URL: https://github.com/MarinusVL/scRNApipe")
whatis("Note: This is actually a conda environment, i.e. it includes many other programs")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/envs/scrnapipe-0.2.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
