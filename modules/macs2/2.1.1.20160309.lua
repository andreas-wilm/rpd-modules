help([[
Model Based Analysis for ChIP-Seq data doesn't seem to work from within snakemake: source activate macs2-2.1.1.20160309 a hack, which might need setting CONDA_DEFAULT_ENV and CONDA_ENV_PATH (or now CONDA_PREFIX)
]])
whatis("Version: 2.1.1.20160309")
whatis("URL: https://github.com/taoliu/MACS/tree/master/MACS2")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/envs/macs2-2.1.1.20160309/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
