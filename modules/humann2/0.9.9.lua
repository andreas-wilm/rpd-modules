help([[
HUMAnN2: The HMP Unified Metabolic Analysis Network 2
]])
whatis("Version: 0.9.9")
whatis("URL: http://huttenhower.sph.harvard.edu/humann2")

-- install to same env
load("metaphlan2/2.6.0")
