help([[
vt is a tool for  A tool set for short variant discovery in genetic sequence data
]])
whatis("Version: 0.577")

whatis("URL: http://genome.sph.umich.edu/wiki/Vt")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "vt-0.577/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
