help([[
PRINSEQ: Easy and rapid quality control and data preprocessing
]])
whatis("Version: lite-0.20.4")
-- whatis("URL: FIXME")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "prinseq-lite-0.20.4/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
