help([[
GNU Emacs - An extensible, customizable, free/libre text editor
]])
whatis("Version: 25.2")
whatis("URL: https://www.gnu.org/software/emacs/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "emacs-25.2/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

d = pathJoin(RpdApps, "emacs-25.2/share/man/")
prepend_path("MANPATH", d)
