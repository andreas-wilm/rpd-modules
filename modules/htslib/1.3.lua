help([[
A C library for reading/writing high-throughput sequencing data
]])
whatis("Version: 1.3")
whatis("URL: http://www.htslib.org/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/htslib-1.3/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
