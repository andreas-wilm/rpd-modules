help([[
Variant Discovery in High-Throughput Sequencing Data
]])
whatis("Version: 3.1.1")
whatis("URL: https://software.broadinstitute.org/gatk/")

load("java/1.7")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/GenomeAnalysisTK-3.1-1/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
