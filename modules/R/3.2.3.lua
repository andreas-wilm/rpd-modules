help([[
R is a free software environment for statistical computing and graphics
]])
whatis("Version: 3.2.3")
-- whatis("URL: FIXME")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "R-3.2.3/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
