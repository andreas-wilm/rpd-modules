help([[
RPD Testing Init
]])

LmodMessage("WARNING: Using the RPD modules is deprecated. RPD vars should be set in init")

-- see also http://lmod.readthedocs.io/en/latest/100_generic_modules.html
-- local d, f = splitFileName(myFileName())
-- assuming this lua file sits ../.. relative to root
-- local root = pathJoin(d, "..", "..")
root = os.getenv("RPD_ROOT")
if root == nil or root == '' then
  LmodError("RPD_ROOT needs to be set first")
end
-- one app folder for all (since separated through modules) should in theory be sufficient
setenv("RPD_APPS", pathJoin(root, "apps.testing"))
setenv("RPD_GENOMES", pathJoin(root, "genomes.testing"))
setenv("RPD_PIPELINES", pathJoin(root, "pipelines"))
-- differentiate between aws and nscc/gis setup,  as long as we still use elm logging
elmdirs = {pathJoin("/scratch", "elm-logs.testing"), pathJoin(root, "elm-logs.testing")}
for _, d in pairs(elmdirs) do
  if isDir(d) then
    setenv("RPD_ELMLOGDIR", d)
  end
end


