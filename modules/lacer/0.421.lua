help([[
Lacer: Recalibration of Illumina base quality scores (Swaine Chen) FIXME won't work outside GIS
]])
whatis("Version: 0.421")

-- whatis("URL: FIXME")
prepend_path("PERL5LIB", "/mnt/software/lib:"/mnt/software/lib/perl5/`perl")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "lacer-0.421/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
