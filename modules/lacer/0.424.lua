help([[
Lacer: Recalibration of Illumina base quality scores (Swaine Chen)
]])
whatis("Version: 0.424")
-- whatis("URL: FIXME")

-- original export PERL5LIB /mnt/software/lib:"/mnt/software/lib/perl5/`perl -e 'printf "%vd", $^V;'`":/mnt/software/share/perl5
-- since it's non trivial to get a return value from a system call in lua we hardcode
v = "5.10.1"
prepend_path("PERL5LIB", "/mnt/software/lib/perl5/" .. v .. ":/mnt/software/share/perl5")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "lacer-0.424/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
