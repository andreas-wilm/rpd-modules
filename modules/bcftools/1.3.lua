help([[
bcftools — utilities for variant calling and manipulating VCFs and BCFs.
]])
whatis("Version: 1.3")
whatis("URL: http://www.htslib.org/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "bcftools-1.3/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
