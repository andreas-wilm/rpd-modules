help([[
Sambamba: process your BAM data faster
]])
whatis("Version: 0.5.9")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "sambamba-v0.5.9/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

        
