help([[
Sambamba: process your BAM data faster
]])
whatis("Version: 0.6.5")
whatis("URL: http://lomereiter.github.io/sambamba/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "sambamba-0.6.5")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
