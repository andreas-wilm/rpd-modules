help([[
Collectively, the bedtools utilities are a swiss-army knife of tools for a wide-range of genomics analysis tasks.
]])
whatis("Version: 2.25.0")
whatis("URL: http://bedtools.readthedocs.io/en/latest/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "bedtools2-2.25.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
