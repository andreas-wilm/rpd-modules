help([[
bedtools: a powerful toolset for genome arithmetic
]])
whatis("Version: 2.17.0")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "bedtools-2.17.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
