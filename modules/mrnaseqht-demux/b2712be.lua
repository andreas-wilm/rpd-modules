help([[
mrnaseqh-demux demuxer for Fluidigm's C1 mRNA Seq HT FIXME perl5 dep
]])
whatis("Version: demux-b2712be")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "mrnaseqht-demux-b2712be/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
