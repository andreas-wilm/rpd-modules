help([[
Dfilter is a generalized signal detection tool for analyzingnext-gen massively-parallel sequencing data by using ROC-AUCmaximizing linear filter
]])
whatis("Version: 2016")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "DFilter2016/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
