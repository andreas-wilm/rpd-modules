help([[
A quality control tool for high throughput sequence data.
]])
whatis("Version: 0.11.4")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "fastqc_v0.11.4/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

	