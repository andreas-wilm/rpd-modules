help([[
Cell Ranger is a set of analysis pipelines that process Chromium single cell 3' RNA-seq output to align reads, generate gene-cell matrices and perform clustering and gene expression analysis
]])
whatis("Version: 2.0.0")
whatis("URL: https://support.10xgenomics.com/single-cell-gene-expression/software/pipelines/latest/what-is-cell-ranger")

RpdApps = os.getenv("RPD_APPS")

d = pathJoin(RpdApps, "cellranger-2.0.0/")
if not isDir(d) then
    LmodError("Directory " .. d .. " does not exist")
end

cmd = "source " .. pathJoin(d, "sourceme.bash")
execute{cmd=cmd, modeA={"load"}}

