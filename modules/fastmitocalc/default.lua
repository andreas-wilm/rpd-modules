help([[
fastMitoCalc -- the ultra-fast version of mitoCalc 
]])
-- whatis("Version: 3.23")
whatis("URL: https://lgsun.irp.nia.nih.gov/hsgu/software/mitoAnalyzer/download.html/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "fastmitocalc/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
