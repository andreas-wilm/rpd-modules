help([[
Stitch contigs together and fill gaps with reference Edit
]])
whatis("Version: b3650b6")
whatis("URL: https://github.com/andreas-wilm/simple-contig-joiner")

load("miniconda3")
load("mummer")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "simple-contig-joiner-b3650b6/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
