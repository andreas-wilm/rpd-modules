help([[
Genetic variant annotation and effect prediction toolbox. 
]])
whatis("Version: 4.2")
whatis("URL: http://snpeff.sourceforge.net/index.html")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "snpeff-4.2")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
