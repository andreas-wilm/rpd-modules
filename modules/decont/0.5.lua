help([[
decont: Decontaminate FastQ files Edit
]])
whatis("Version: 0.5")
whatis("URL: https://github.com/CSB5/decont")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "decont-0.5/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
