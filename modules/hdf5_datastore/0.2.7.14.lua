help([[
hdf5_datastore for SRAQuery
]])
-- whatis("Version: 0.2.7.14")
-- whatis("URL: ")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "hdf5_datastore-0.2.7.14/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
