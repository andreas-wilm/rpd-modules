help([[
bamUtil is a repository that contains several programs that perform operations on SAM/BAM files. All of these programs are built into a single executable, bam.
]])
whatis("Version: 1.0.14")
whatis("URL: http://genome.sph.umich.edu/wiki/BamUtil")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "bamUtil-1.0.14/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
