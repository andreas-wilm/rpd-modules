help([[
seqtk is a toolkit for processing sequences in FASTA/Q formats
]])
whatis("Version: 4feb6e8")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "seqtk-4feb6e8/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
