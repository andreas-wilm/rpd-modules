help([[
UCSC genome browser 'kent' bioinformatic utilities
]])
whatis("Version: 338")
whatis("URL: https://github.com/bowhan/kent/tree/master/src/userApps")

RpdApps = os.getenv("RPD_APPS")
d= pathJoin(RpdApps, "/ucsc-userapps-v338/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end

