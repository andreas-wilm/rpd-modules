help([[
Bowtie 2 is an ultrafast and memory-efficient tool for aligning sequencing reads to long reference sequences
]])
whatis("Version: 2.3.0")
whatis("URL: http://bowtie-bio.sourceforge.net/bowtie2/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/bowtie2-2.3.0/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
