help([[
Miniconda3
]])
-- whatis("Version: 0.7.12")
whatis("URL: http://conda.pydata.org/miniconda.html")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "miniconda3/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
