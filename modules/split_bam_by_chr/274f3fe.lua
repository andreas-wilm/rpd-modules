help([[
split BAM by chromosome (plus unaligned)
]])
whatis("Version: 274f3fe")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "split_bam_by_chr-274f3fe/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
