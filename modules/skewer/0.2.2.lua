help([[
skewer implements the bit-masked k-difference matchingalgorithm dedicated to the task of adapter trimming
]])
whatis("Version: 0.2.2")

whatis("URL: https://github.com/relipmoc/skewer")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "skewer-0.2.2/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
