help([[
GCC, the GNU Compiler Collectiondk_alter PATH $RPD_APPS/gotcloud.1.17/bindk_alter PATH $RPD_APPS/gotcloud.1.17/dk_alter LD_LIBRARY_PATH  /opt/gcc-4.9.3/lib64/
]])
whatis("Version: 4.7.2")
-- whatis("URL: FIXME")

RpdApps = os.getenv("RPD_APPS")
prepend_path("LD_LIBRARY_PATH", pathJoin(RpdApps, "gcc-4.7.2/lib64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(RpdApps, "gcc-4.7.2/lib"))

d = pathJoin(RpdApps, "gcc-4.7.2/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
