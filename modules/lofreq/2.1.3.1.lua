help([[
LoFreq Star (i.e. LoFreq version 2) is a fast and sensitive variant- caller for inferring SNVs and indels from next-generation sequencing data. conda env python-2.7 set as shebang and not needed here
]])
whatis("Version: 2.1.3.1")
whatis("URL: http://csb5.github.io/lofreq/")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "lofreq_star-2.1.3.1/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
