help([[
This is a hack to compute coverage from precomputed BAM files for SG10K
]])
whatis("Version: 060217")
whatis("URL: https://bitbucket.org/andreas-wilm/sg10k-cov")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "sg10k-cov-062017/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
