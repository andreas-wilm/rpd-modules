help([[
RNA-seq aligner
]])
whatis("Version: 2.5.3a")
whatis("URL: https://github.com/alexdobin/STAR")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/star-2.5.3a/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
