help([[
vcflib is a simple C++ library for parsing and manipulating VCF files, + many command-line utilities
]])
whatis("Version: 66cb570")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "vcflib-66cb570/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
	
