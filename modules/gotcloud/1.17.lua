help([[
Genomes on the Cloud, Mapping & Variant Calling Pipelines
]])
whatis("Version: 1.17")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "gotcloud.1.17/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
