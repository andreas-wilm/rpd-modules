help([[
The MEME suite: Motif-based sequence analysis tools
]])
whatis("Version: 4.11.2")

-- whatis("URL: FIXME")
RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "meme-4.11.2/bin")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
