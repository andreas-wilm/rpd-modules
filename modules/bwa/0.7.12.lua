help([[
BWA is a software package for mapping low-divergent sequences against a large reference genome
]])
whatis("Version: 0.7.12")
whatis("URL: http://bio-bwa.sourceforge.net/")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "/bwa-0.7.12/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
