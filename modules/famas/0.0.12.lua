help([[
famas: Yet another fastq massaging program Edit
]])
whatis("Version: 0.0.12")
whatis("URL: https://github.com/andreas-wilm/famas")

RpdApps = os.getenv("RPD_APPS")
d = pathJoin(RpdApps, "famas-0.0.12/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
