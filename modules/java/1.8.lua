help([[
OpenJDK (Open Java Development Kit) is a free and open source implementation of the Java Platform, Standard Edition (Java SE)
]])
whatis("Version: 1.8")
whatis("URL: http://openjdk.java.net/")


RpdApps = os.getenv("RPD_APPS")
-- Try several dirs
dirs = {pathJoin(RpdApps, "jre1.8.0_112/"), "/usr/lib/jvm/jre-1.8.0"}
for _, d in ipairs(dirs) do
    -- test bin because d might be symlink
    if isDir(pathJoin(d, "/bin/")) then
        javahome = d
	break
    end
end
setenv("JAVA_HOME", javahome)

d = pathJoin(javahome, "/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
