help([[
OpenJDK (Open Java Development Kit) is a free and open source implementation of the Java Platform, Standard Edition (Java SE)
]])
whatis("Version: 1.7")
whatis("URL: http://openjdk.java.net/")

JavaHome = "/usr/lib/jvm/jre-1.7.0"
setenv("JAVA_HOME", JavaHome)

d = pathJoin(JavaHome, "/bin/")
if isDir(d) then
    prepend_path("PATH", d)
else
    LmodError("Directory " .. d .. " does not exist")
end
        
